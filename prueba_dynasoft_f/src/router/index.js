import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',

    name: 'Home',
    //component: () => import(/* webpackChunkName: "about" */ '../views/Home.vue')
    component: () => import(/* webpackChunkName: "about" */ '../views/Comercios.vue')
  },
  {
    path: "/Detallecomercio",
    name: "Detalles comercio",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/Detallecomercio.vue"),
  },


]

const router = new VueRouter({
  routes
})

export default router
