
import Tipo_comercio from '../modelos/tipo_comercio'
import Categoria from '../controllers/categoria_controller'
const tipc={};
const tipo = {};


tipo.getTipos = async (req, res) => {

  try {
    const tipos = await Tipo_comercio.findAll({
      })
    res.json({
      data: tipos
    })
  } catch (error) {
    console.log(error);
    res.json({
      data: {},
      message: 'la accion no ha podido realizarse'
    });
  }
};

tipo.OneTipo=async (req, res) =>{
  const { id_tipo } = req.params;
  try {
      const tipos= await Tipo_comercio.findOne({
          where: {
              id_tipo
          }
      })
      res.json(tipos);
  } catch (error) {
      console.log(error);
  }
}


module.exports = tipo;


