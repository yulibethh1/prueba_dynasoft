
import Categoria from '../modelos/categoria'


const categoria = {};
categoria.getCategorias = async (req, res) => {
   const categorias = await Categoria.findAll()
   res.json({

      data: categorias
   });

};
categoria.OneCategoria=async (req, res) =>{
   const { id_tipo } = req.params;
   try {
       const categorias= await Categoria.findAll({
           where: {
               id_tipo
           }
       })
       res.json(categorias);
   } catch (error) {
       console.log(error);
   }
 }

module.exports = categoria;
