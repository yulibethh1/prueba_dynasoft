import Comercio from '../modelos/comercio';
const comercio = {};

comercio.getComercios = async (req, res) => {   //mostar comercios existentes  
 
  try {
    const comercios = await Comercio.findAll({
      atributes: ['id_comercio', 'nombre_comercio', 'fecha_creacion', 'nombre_dueno', 'direccion'
        , 'tipo_comercio', 'catgoria_id']
    });
    res.json({
      data: comercios
      
    })
  } catch (error) {
    console.log(error);
    res.json({
      data: {},
      message: 'la accion no ha podido realizarse'
    });
  }
};

comercio.getOneComercio=async (req, res) =>{
  const { id_comercio } = req.params;
  try {
      const comercios = await Comercio.findOne({
          where: {
              id_comercio
          }
      })
      res.json(comercios);
  } catch (error) {
      console.log(error);
  }
}

comercio.createComercio = async (req, res) => {
  try {
    const { nombre_comercio, fecha_creacion, nombre_dueno, direccion, comercio_tipo, catgoria_id } = req.body;
    let nuevoComercio = await Comercio.create({
      nombre_comercio,
      fecha_creacion,
      nombre_dueno,
      direccion,
      comercio_tipo,
      catgoria_id

    }, {
      fields: ['nombre_comercio', 'fecha_creacion', 'nombre_dueno', 'direccion', 'comercio_tipo', 'catgoria_id']
    });
    if (nuevoComercio) {
      return res.json({
        message: 'Comercio creado con exito',


      });
    }


  } catch (error) {
    console.log(error);
    res.status(500).json({
      message: 'no se ha podido ejecutar ',


    });

  }
}
comercio.updateComercio = async (req, res) => {
  const { id_comercio } = req.params;
  const { nombre_comercio, fecha_creacion, nombre_dueno, direccion, comercio_tipo, catgoria_id } = req.body;
  try {
    const comercios = await Comercio.findAll({
      atributes: ['nombre_comercio', 'fecha_creacion', 'nombre_dueno', 'direccion', 'comercio_tipo', 'catgoria_id'],
      where: {
        id_comercio
      }
    });
    if (comercios.length > 0) {
      comercios.forEach(async (comercio) => {
        await comercio.update({

          nombre_comercio,
          fecha_creacion,
          nombre_dueno,
          direccion,
          comercio_tipo,
          catgoria_id
        });
      });
      return res.json({
        message: 'Comercio Updated',
        data: comercios
      })
    }
  } catch (e) {
    res.json({
      message: 'la accion no se ha podido Realizar',
      data: {}
    })
  }
};

comercio.deleteComercio = async (req, res) => {
  const { id_comercio } = req.params;
  try {
    await Comercio.destroy({
      where: {
         id_comercio
      }
    });
    const deleteRowsCount = await Comercio.destroy({
      where: {
        id_comercio
      }
    });
    res.json({
      message: 'Comercio Eliminado',
      count: deleteRowsCount
    })
  } catch (error) {
    res.json({
      message: 'eliminacion fallida',
      data: {}
    });
  }
};
module.exports = comercio;


