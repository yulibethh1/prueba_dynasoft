const Comercio=require('./comercio');
const Tipo_comercio= require('./tipo_comercio');
const Categoria =require('./categoria');

// relacion comercio-tipo :  uno-uno
Comercio.belongsTo(Tipo_comercio);
Tipo_comercio.hasOne(Comercio);

//relacion tipo-categoria:  uno:uno
Categoria.belongsTo(Tipo_comercio, { foreingKey: "id_tipo" });
Tipo_comercio.hasOne(Categoria, { foreingKey: "id_tipo" });

// relacion comercio-categoria: muchos_muchos
Comercio.belongsToMany(Categoria,{through:"comercio_categoria"});
Categoria.belongsToMany(Comercio,{through:"comercio_categoria"});


