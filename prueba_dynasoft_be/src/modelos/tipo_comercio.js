
import Sequelize from 'sequelize';
import { sequelize } from '../database/database';



const Categoria = require('./categoria');
const Comercio = require('./comercio');

const Tipo_comercio = sequelize.define('tipo_comercio',
    {
        id_tipo: {
            type: Sequelize.INTEGER,
            primaryKey: true

        },
        nombre_tipo: {
            type: Sequelize.STRING
        },


    },
    {
        timestamps: false,
        freezeTableName: true,
        
    }

);
module.exports=Tipo_comercio;


