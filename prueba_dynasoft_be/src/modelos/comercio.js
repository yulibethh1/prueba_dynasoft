import Sequelize from 'sequelize';
import { sequelize } from '../database/database';


const Tipo_comercio = require('./tipo_comercio');
const Categoria=require('./categoria');

const Comercio = sequelize.define('comercio',
{
    id_comercio:{
        type: Sequelize.INTEGER,
        primaryKey:true,
        ondelete:'CASCADE',
        references:{
            module:'comercio',
            key:'comercio_tipo'
        }
        
    },
    nombre_comercio:{
        type:Sequelize.STRING
    },

    fecha_creacion:{
        type:Sequelize.DATE
    },

    nombre_dueno:{
        type:Sequelize.STRING
    },
    direccion:{
        type:Sequelize.STRING
    },
    comercio_tipo:{
        type:Sequelize.INTEGER,
        onDelete:'CASCADE',
        references:{
            module:"tipo_comercio",
            key:"id_tipo"

        }
    },
    catgoria_id:{
        type:Sequelize.INTEGER,
        onDelete:'CASCADE',
        references:{
            module:"categoria",
            key:"id_tipo"
    },
}
    
},{
    timestamps:false,
    freezeTableName: true,
    
}
);

module.exports=Comercio;


