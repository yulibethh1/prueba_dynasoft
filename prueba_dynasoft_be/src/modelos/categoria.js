
import Sequelize from 'sequelize';
import { sequelize } from '../database/database';
//import Comercio from './comercio';

const Comercio = require('./comercio');

const Categoria = sequelize.define('categoria',
    {
        id_categoria: {
            type: Sequelize.INTEGER,
            primaryKey: true
        },
        nombre_categoria: {
            type: Sequelize.STRING
        },

        id_tipo: {
            type: Sequelize.INTEGER
        },


    }, {
    timestamps: false,
    freezeTableName: true,
    
}
);

module.exports = Categoria;

