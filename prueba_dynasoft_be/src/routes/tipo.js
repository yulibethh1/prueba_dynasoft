
import express from 'express';
import tipo from '../controllers/seleccion_controler';

const router = express.Router();
router.post('/api/tipo',tipo.getTipos);
router.post('/tipo/:id_tipo',tipo.OneTipo);



module.exports = router;
