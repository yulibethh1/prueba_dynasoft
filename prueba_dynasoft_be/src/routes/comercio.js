
import express from 'express';
import comercio from '../controllers/comercio_controller';

const router = express.Router();

router.get("/api/comercio/", comercio.getComercios);
router.post("/comercio", comercio.createComercio);
router.post("/one/comercio/:id_comercio", comercio.getOneComercio);
router.put("/api/comercio/:id_comercio", comercio.updateComercio);
router.delete("/api/dcomercio/:id_comercio", comercio.deleteComercio);
router.get('/:id_comercio',async(req,res)=>{
  const comercio=  await comercio.findOne(req.params.id_comercio);
  res.json(comercio);
});



module.exports = router;

