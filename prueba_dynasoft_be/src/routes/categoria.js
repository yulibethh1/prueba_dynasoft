
import express from 'express';
import categoria from '../controllers/categoria_controller';

const router = express.Router();

router.post('/api/categoria', categoria.getCategorias);

router.post('/categoria/:id_tipo',categoria.OneCategoria);

module.exports = router;

