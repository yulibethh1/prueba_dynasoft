import express, { json } from 'express';
import morgan from 'morgan';
import cors from 'cors';
import history from 'connect-history-api-fallback';
import path from 'path';
import '@babel/polyfill';

//INICIALIZACION 
const app = express();

//MIDDLEWARES
app.use(morgan('tiny'));
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(history());
app.use(express.static(path.join(__dirname, 'public')));

//Routes 

app.use('/api', require('./routes/comercio'));
app.use('/api',require('./routes/tipo'));
app.use('/api',require('./routes/categoria'));

//app.use('/', require('./routes/categoria'));

// route static
app.use(express.static(__dirname + '/public/routes/comercio'));


;
//SERVIDOR

app.set('port', process.env.PORT || 3000);

app.listen(app.get('port'), () => {
    console.log('servidor en el puerto ' + app.get('port'));

});

export default app;